const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());
const port = 6062;
app.listen(port, () => {
  console.log("Server online on: " + port);
});
app.use("/", express.static("../front-end"));

const connectionSoul = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "db_seal_your_souls",
});
connectionSoul.connect(function (err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS souls(nume VARCHAR(255), prenume  VARCHAR(255), telefon  VARCHAR(255) , email  VARCHAR(255), facebook VARCHAR(255), gen VARCHAR(10))";
  connectionSoul.query(sql, function (err, result) {
    if (err) throw err;
  });
});

const connectionGhostly = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "db_ghostly_affair",
});
connectionGhostly.connect(function (err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS participanti(nume VARCHAR(255), prenume  VARCHAR(255), telefon  VARCHAR(255) , email  VARCHAR(255), facebook VARCHAR(255))";
  connectionGhostly.query(sql, function (err, result) {
    if (err) throw err;
  });
});

app.post("/student", (req, res) => {
  let pasager = {
    nume: req.body.nume,
    prenume: req.body.prenume,
    telefon: req.body.telefon,
    email: req.body.email,
    facebook: req.body.facebook,
    gen: req.body.gen,
  };

  let error = [];
  if (!pasager.nume || !pasager.prenume || !pasager.telefon || !pasager.email || !pasager.facebook || !pasager.gen) {
    console.log("Unul sau mai multe campuri nu au fost introduse!");
    error.push("Unul sau mai multe campuri nu au fost introduse!");
  }
  else {
    //validare nume
    if (pasager.nume.length < 3 || pasager.nume.length > 30) {
      console.log("Nume invalid!");
      error.push("Nume invalid!");
    }
    else if (!(pasager.nume.match("^[A-Za-z]+$"))) {
      console.log("Numele trebuie sa contina doar litere!");
      error.push("Numele trebuie sa contina doar litere!");
    }
    else if (pasager.nume.charAt(0) !== pasager.nume.charAt(0).toUpperCase()) {
      console.log("Numele trebuie sa inceapa cu majuscula!");
      error.push("Numele trebuie sa inceapa cu majuscula!");
    }
    //validare prenume
    if (pasager.prenume.length < 3 || pasager.prenume.length > 30) {
      console.log("Prenume invalid!");
      error.push("Prenume invalid!");
    }
    else if (!(pasager.prenume.match("^[A-Za-z -]+$"))) {
      console.log("Prenumele poate contine doar litere, spatii si cratima. ");
      error.push("Prenumele poate contine doar litere, spatii si cratima.")

    }
    //validare telefon
    if (!(pasager.telefon.match("^[0-9]+$"))) {
      console.log("Numarul de telefon poate contine doar cifre!");
      error.push("Numarul de telefon poate contine doar cifre!");
    }
    else if (pasager.telefon.length != 10) {
      console.log("Numar de telefon invalid!");
      error.push("Numar de telefon invaid!");
    }
    //validare email
    if (!(pasager.email.includes("@gmail.com") || pasager.email.includes("@yahoo.com") || pasager.email.includes("@stud.ase.ro"))) {
      console.log("Email invalid!");
      error.push("Email invalid!");
    }
    //validare facebook
    if (!(pasager.facebook.includes("www.facebook.com"))) {
      console.log("Link de Facebook invalid!");
      error.push("Link de Facebook invalid!");
    }
  }

  if (error.length === 0) {

    const sql = `INSERT INTO souls (
      nume,
      prenume,
      telefon,
      email,
      facebook,
      gen) VALUES (?,?,?,?,?,?)`;
    connectionSoul.query(
      sql,
      [
        pasager.nume,
        pasager.prenume,
        pasager.telefon,
        pasager.email,
        pasager.facebook,
        pasager.gen
      ],
      function (err, result) {
        if (err) throw err;
        console.log("Te-ai inscris cu succes!");
        res.status(200).send({
          message: "Te-ai inscris cu succes!",
        });
        console.log(sql);
      }
    );
  } else {
    res.status(500).send(error);
    console.log("A aparut o problema!");
  }
  app.use('/', express.static('../front-end'))
});

app.post("/participant", (req, res) => {
  let pasager = {
    nume: req.body.nume,
    prenume: req.body.prenume,
    telefon: req.body.telefon,
    email: req.body.email,
    facebook: req.body.facebook,
  };

  let error = [];
  if (!pasager.nume || !pasager.prenume || !pasager.telefon || !pasager.email || !pasager.facebook) {
    console.log("Unul sau mai multe campuri nu au fost introduse!");
    error.push("Unul sau mai multe campuri nu au fost introduse!");
  }
  else {
    //validare nume
    if (pasager.nume.length < 3 || pasager.nume.length > 30) {
      console.log("Nume invalid!");
      error.push("Nume invalid!");
    }
    else if (!(pasager.nume.match("^[A-Za-z]+$"))) {
      console.log("Numele trebuie sa contina doar litere!");
      error.push("Numele trebuie sa contina doar litere!");
    }
    else if (pasager.nume.charAt(0) !== pasager.nume.charAt(0).toUpperCase()) {
      console.log("Numele trebuie sa inceapa cu majuscula!");
      error.push("Numele trebuie sa inceapa cu majuscula!");
    }
    //validare prenume
    if (pasager.prenume.length < 3 || pasager.prenume.length > 30) {
      console.log("Prenume invalid!");
      error.push("Prenume invalid!");
    }
    else if (!(pasager.prenume.match("^[A-Za-z -]+$"))) {
      console.log("Prenumele poate contine doar litere, spatii si cratima. ");
      error.push("Prenumele poate contine doar litere, spatii si cratima.")

    }
    //validare telefon
    if (!(pasager.telefon.match("^[0-9]+$"))) {
      console.log("Numarul de telefon poate contine doar cifre!");
      error.push("Numarul de telefon poate contine doar cifre!");
    }
    else if (pasager.telefon.length != 10) {
      console.log("Numar de telefon invalid!");
      error.push("Numar de telefon invaid!");
    }
    //validare email
    if (!(pasager.email.includes("@gmail.com") || pasager.email.includes("@yahoo.com") || pasager.email.includes("@stud.ase.ro"))) {
      console.log("Email invalid!");
      error.push("Email invalid!");
    }
    //validare facebook
    if (!(pasager.facebook.includes("www.facebook.com"))) {
      console.log("Link de Facebook invalid!");
      error.push("Link de Facebook invalid!");
    }
  }

  if (error.length === 0) {

    const sql = `INSERT INTO participanti (
      nume,
      prenume,
      telefon,
      email,
      facebook) VALUES (?,?,?,?,?)`;
    connectionGhostly.query(
      sql,
      [
        pasager.nume,
        pasager.prenume,
        pasager.telefon,
        pasager.email,
        pasager.facebook
      ],
      function (err, result) {
        if (err) throw err;
        console.log("Te-ai inscris cu succes!");
        res.status(200).send({
          message: "Te-ai inscris cu succes!",
        });
        console.log(sql);
      }
    );
  } else {
    res.status(500).send(error);
    console.log("A aparut o problema!");
  }
  app.use('/', express.static('../front-end'))
});

